import Vue from "vue";


Vue.filter("decimals", (value, decimals = 2) => {
    return value.toFixed(decimals).toLocaleString();
});

Vue.filter("money", (value, symbol = '') => {
    return symbol +' '+ value;
});

Vue.filter('phone', function (phone) {
    return phone.toString().replace(/[^0-9]/g, '')
                .replace(/(\d{4})(\d{4})(\d{3})/, '($1)-$2.$3');
});