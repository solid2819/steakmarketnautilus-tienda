/**
 * Servicio de Monedas
 * 
 * @author Brayan Rincon <brayan262@gmail.com>
 * @extends Model
 * @version 1.0.0
 * 
 * ```js
 * // Devuelve todos los registros
 * instance.all().then( response => { if( response.status == '200' ) return true; } )
 * ```
 */
export default class LocalStorageManager {

    _key;

    constructor(key) {
        this._key = key
    }

    get(key, value = null) {


        let storage = JSON.parse(localStorage.getItem(this._key))
        return storage;

        if( typeof storage[key] === 'undefined' ) {
            storage[key] = value
            localStorage.setItem('smn', storage)
            return value;
        }

        return storage[key];
    }

    getJSON(key, value = null) {
        return JSON.parse( this.get(key, value) )
    }

    set(key, value) {
        
        let storage = JSON.parse(localStorage.getItem(this._key))
        storage[key] = value; 
        return value
        
        /*
        try {
            item = localStorage.getItem(this._key)
            item = localStorage.setItem(this._key, value)
        } catch (error) {
            return false;
        }
        
        var storage = JSON.parse(item)
        return storage[key]*/
    }


}