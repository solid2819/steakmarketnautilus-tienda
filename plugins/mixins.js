import Vue from 'vue'
import Config from "../data/config.json";
import { $api } from "@/services";

const state = {
    config: Config.config.api,
    slug: Config.config.slug,
    config: Config.config,
}


Vue.prototype.$config0 = state.config

Vue.mixin({
    data() {
        return {
            config: state.config,
            $config0: state.config
        }
    },
    computed: {
        $api: () => $api,
        //$conf: () => state.config,
        $config: () => state.config
    }
})
 