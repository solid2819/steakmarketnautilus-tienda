import store from '@/store' // your vuex store

function trailingSlash (str) {
    return str.endsWith('/') ? str : str + '/'
}

export default ({ store  }) => {
    store.app.router.beforeEach((to, from, next) => {
        if( to.matched.some(record => record.path == "/mi-cuenta/dashboard") ) {
          if (store.getters['auth/isAuth']) {
            return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
          }
          next('/mi-cuenta/login')
        }
        else {
          return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
        }
      
    })
}