import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'smn',
    paths: [
      'cart',
      'currency',
      //'products',
      'categories',
      'auth'
    ]
  })(store)
}
