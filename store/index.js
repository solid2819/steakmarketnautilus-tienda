import Vue from 'vue'
import Vuex from 'vuex'
import blog from './modules/blog'
import menu from './modules/menu'
import currency from './modules/currency'
import address from './modules/adresses'
import products from './modules/products'
import cart from './modules/cart'
import filter from './modules/filter'
import layout from './modules/layout'
import settings from './modules/settings'
import categories from './modules/categories'
import paymentsMethods from './modules/paymentsMethods'

import countries from './modules/countries'
import states from './modules/states'
import cities from './modules/cities'

import auth from './modules/auth'
import Config from "../data/config.json";
import storePlugins from "@/plugins/storePlugins";
import { $api } from "@/services";

Vue.use(Vuex)

/*const state = {
  config: Config.config.api,
  slug: Config.config.slug,
  config: Config.config,
}*/


const store = () => {
  return new Vuex.Store({
    plugins: [storePlugins],
    //state: state,
    modules: {
      settings,
      blog,
      menu,
      currency,
      categories,
      products,
      address,
      cart,
      filter,
      layout,
      paymentsMethods,
      auth,
      countries,
      states,
      cities
    }
  })
}

export default store
