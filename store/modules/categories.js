import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

//to handle state
const state = {
    data: [],

    roots: [],

    /** Evita que se solicite la API multiples veces */
    loading: {
        data: false,
        roots: false,
    }
}

//to handle state
const getters = {
    data (state){
        return state.data
    },

    roots (state){
        return state.roots
    },

    getByslug: (state) => (slug) => {
        return state.roots.find(item => item.slug == slug)
    }
}

//to handle actions
const actions = {
    async all({ commit }) {
        if( state.data.length < 1 && state.loading.data == false) { // Veriicar si hay datos en cache
            commit('SET_LOADING', {loading: 'data', value: true}) // Impedir nuevas solicitudes
            await this.$api.categories.all().then(response => {
                commit('SET_DATA', response.data)
            });
        }
    },

    async getCategoriesRoot({ commit }) {
        if( state.roots.length < 1 && state.loading.roots == false) { // Veriicar si hay datos en cache
            commit('SET_LOADING', {loading: 'roots', value: true}) // Impedir nuevas solicitudes
            await this.$api.categories.getCategoriesRoot().then(response => {
                commit('SET_CATEGORIES_ROOT', response.data)
            });
        }
    },

    async getProductsOfCategory({ commit }) {
        if( state.roots.length < 1 && state.loading.roots == false) { // Veriicar si hay datos en cache
            commit('SET_LOADING', {loading: 'roots', value: true}) // Impedir nuevas solicitudes
            await this.$api.categories.getCategoriesRoot().then(response => {
                commit('SET_CATEGORIES_ROOT', response.data)
            });
        }
    }
}

//to handle mutations
const mutations = {
    SET_LOADING: (state, data) => {
        state.loading[data.loading] = data.value
    },

    SET_DATA(state, data) {
        state.data = data
        state.loading.data = false
    },
    SET_CATEGORIES_ROOT(state, data) {
        state.roots = data
        state.loading.roots = false
    }
}

//export store module
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
