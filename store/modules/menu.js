import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
  /** Datos en cache */
  data: [],

  categories: [],

  /** Evita que se solicite la API multiples veces */
  loading: {
    data: false,
    categories: false
  }
}

// getters
const getters = {

  data (state){
    return state.data
  },

  categories (state){
    return state.categories
  }

}

// actions
const actions = {

  async all({ commit }) {
    if( state.data.length < 1 && state.loading.data == false) { // Veriicar si hay datos en cache
        state.loading.data = true
        await this.$api.menu.all().then(response => {
            commit('SET_DATA', response.data)
        });
    }
  },

  async getCategories({ commit }) {
    if( state.categories.length < 1 && state.loading.categories == false) { // Veriicar si hay datos en cache
        state.loading.categories = true
        await this.$api.menu.getCategories().then(response => {
            commit('SET_CATEGORIES', response.data)
        });
    }
  },

}

// mutations
const mutations = {

  SET_DATA(state, data) {
    state.data = data
    state.loading.data = false
  },

  SET_CATEGORIES(state, data) {
    state.categories = data
    state.loading.categories = false
  }
}

//export store module
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
