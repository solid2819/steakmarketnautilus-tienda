import Vue from "vue";
import { API, $api } from "@/services";
import ls from '@/helpers/localStorage'

const user = ls.get('auth-user') || '{}';
const token = ls.get('auth-token') || false;

const state = {

  data: [],

  filterList:[],

  user: user,

  token: token,

  progress: 0,

  /** Evita que se solicite la API multiples veces */
  loading: {
    data: false,
    login: false
  },

  notifications: [],

}

const getters = {

  pending: (state) => (sender) => {
    return state.loading[sender]
  },

  progress(state){
    return state.progress;
  },

  loading(state){
    return state.loading.data;
  },

  data (state){
    return state.data
  },

  count (state){
    return state.data.length
  },

  isAuth(state) {
    return state.token == false ? false : true
  },

  user (state){
    return state.user
  },

  filterList (state){
    return state.filterList
  },

}

const actions = {

  /**
   * Actualiza el progreso de consulta a la API
   * @param {Object} commit
   * @param {Number} payload Progreso
   */
  login ({ commit }, payload){

    let _payload = payload

    return new Promise((resolve, reject) => {

      if( state.loading.login == false) { // Veriicar si hay datos en cache

        //commit('REQUEST', 'login') // Impedir nuevas solicitudes

        this.$api.users.login(_payload)
          .then(response => {
            if( response.status == 'success' ) {

              //commit('RESPONSE', 'login')
              commit('LOGIN', response)

              ls.set('auth-user', response.data)
              ls.set('auth-token', response.token)
              resolve(response)
            }
            else {
              ls.rem('auth-user')
              ls.rem('auth-token')
              commit('RESPONSE', 'login')
              reject(response.data)
            }
          }) // then
          .catch(error => {
            // eslint-disable-next-line no-console
            console.log(error);
            ls.rem('auth-token')
            commit('RESPONSE', 'login')
            reject(error);
          }); // catch

      } // if
    }); // Promise
  },

  /**
   * Actualiza un registro
   * @param {Object} commit
   * @param {Object} data
   * @returns {Promise} response
   */
  async forgot ({ commit, state }, email) {

    console.log(6)

    return new Promise((resolve, reject) => { // Crear Promesa

      //state.service.progress() // Habilitar preogreso de consulta a la API
      //commit('REQUEST', 'forgot')

      this.$api.users.forgot(email)
        .then(response => {
          //commit('PROGRESS', 0)
          //commit('RESPONSE', 'forgot')
          resolve(response)
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          //commit('RESPONSE', 'forgot')
          //commit('PROGRESS', 0)
          reject(error);
        }); // catch
    });
  },


  /**
   * Actualiza un registro
   * @param {Object} commit
   * @param {Object} data
   * @returns {Promise} response
   */
  async reset ({ commit, state }, data) {

    return new Promise((resolve, reject) => { // Crear Promesa

      //this.$api.users.progress() // Habilitar preogreso de consulta a la API
      //commit('REQUEST', 'reset')

      this.$api.users.reset(data)
        .then(response => {
          //commit('PROGRESS', 0)
          //commit('RESPONSE', 'reset')
          resolve(response)
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          //commit('RESPONSE', 'reset')
          //commit('PROGRESS', 0)
          reject(error);
        }); // catch
    });
  },


  /**
   * Actualiza el progreso de consulta a la API
   * @param {Object} commit
   * @param {Number} payload Progreso
   */
  updateProfile ({ commit }, payload){

    let _payload = payload

    return new Promise((resolve, reject) => {

      if( state.loading.login == false) { // Veriicar si hay datos en cache

        //commit('REQUEST', 'login') // Impedir nuevas solicitudes

        this.$api.users.update(_payload.id, _payload)
          .then(response => {
            if( response.status == 'success' ) {
              commit('LOGIN', response)
              ls.set('auth-user', response.data)
              resolve(response)
            }
            else {
              ls.rem('auth-user')
              ls.rem('auth-token')
              commit('RESPONSE', 'login')
              reject(response.data)
            }
          }) // then
          .catch(error => {
            // eslint-disable-next-line no-console
            console.log(error);
            ls.rem('auth-token')
            commit('RESPONSE', 'login')
            reject(error);
          }); // catch

      } // if
    }); // Promise
  },

}

const mutations =  {

  /**
   * Actualiza el progreso de consulta a la API
   * @param {Object} state
   * @param {Number} payload Progreso
   */
  PROGRESS (state, payload) {
    state.progress = payload
  },

  /**
   * Establece una petición al servidor
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  REQUEST (state, payload) {
    Vue.set(state.loading, payload, true);
  },

  /**
   * El que el servidor ha respondido
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  RESPONSE (state, payload) {
    Vue.set(state.loading, payload, false);
  },

  /**
   * Almacenar los registros devueltos por la API
   *
   * @param {Object} state
   * @param {Object} payload DAtos devueltos
   */
  LOGIN (state, payload) {
    state.user = payload.data;
    state.token = payload.token;
  },


  /**
   * Actualizar los registro actuales con el último actualizado
   *
   * @param {Object} state
   * @param {Object} payload Registro actualizado
   */
  UPDATE (state, data) {
    var index = state.data.findIndex(item => item.id == data.id)
    state.data[index] = data
    state.data = [ ...state.data ]
    state.filterList = state.data
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
