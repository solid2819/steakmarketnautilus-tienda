
const state = {
  data: [],
  shipping: [],
  stores: [
    {
      id: -1,
      name: "STEAK MARKET NAUTILUS",
      identification: "",
      phone: "(0123) 456 7891",
      address: "Steak Market Nautilus, Calle 148, Valencia 2001, Carabobo",
      billding: 1
    }
  ],
  loading: {
    data: false,
    billding: null
  },
}

// getters
const getters = {

  data: (state) => {
    return state.data;
  },

  stores: (state) => {
    return state.stores;
  },

  billding: (state) => {
    return state.billding;
  },

  shipping: (state) => {
    return state.shipping;
  },
}

// actions
const actions = {

  async all ({ commit }) {
    if( state.data.length < 1 && state.loading.data == false) { // Veriicar si hay datos en cache
      commit('SET_LOADING', {loading: 'data', value: true}) // Impedir nuevas solicitudes
      await this.$api.addresses.getAddressByUser().then(response => {
          commit('SET_DATA', response.data)
      });
    }
  },

  /**
   * Crea un nuevo registro
   * @param {Object} commit
   * @param {Object} data
   * @returns {Promise} response
   */
  async store ({ commit }, data) {

    return new Promise((resolve, reject) => { // Crear Promesa

      //this.$api.addresses.progress() // Habilitar preogreso de consulta a la API

      this.$api.addresses.store(data)
        .then(response => {
          if( response.status == 200 ){ // Si se proceso con exito
            commit('STORE', response.data)
            commit('PROGRESS', 0)
            resolve(response)
          }
          else {
            commit('PROGRESS', 0)
            reject(response) // Se produjo un error
          }
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          commit('PROGRESS', 0)
          reject(error);
        }); // catch
    });

  },


  /**
   * Actualiza un registro
   * @param {Object} commit
   * @param {Object} data
   * @returns {Promise} response
   */
  async update ({ commit }, data) {

    return new Promise((resolve, reject) => { // Crear Promesa

      //this.$api.addresses.progress() // Habilitar preogreso de consulta a la API

      this.$api.addresses.update(data.id, data)
        .then(response => {
          if( response.status == 200 ){ // Si se proceso con exito
            commit('UPDATE', response.data)
            commit('PROGRESS', 0)
            resolve(response)
          } else {
            reject(response) // Se produjo un error
          }
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          commit('PROGRESS', 0)
          reject(error);
        }); // catch
    });
  },


  /**
   * Actualiza un registro
   * @param {Object} commit
   * @param {Object} data
   * @returns {Promise} response
   */
   async destroy ({ commit }, id) {
    let _id = id
    return new Promise((resolve, reject) => { // Crear Promesa

      //this.$api.addresses.progress() // Habilitar preogreso de consulta a la API

      this.$api.addresses.destroy(_id)
        .then(response => {
          if( response.status == 200 ){ // Si se proceso con exito
            commit('DESTROY', response.data)
            commit('PROGRESS', 0)
            resolve(response)
          } else {
            reject(response) // Se produjo un error
          }
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          commit('PROGRESS', 0)
          reject(error);
        }); // catch
    });
  },


}


// mutations
const mutations = {
  SET_LOADING: (state, data) => {
    state.loading[data.loading] = data.value
  },

  SET_DATA: (state, data) => {
    state.data = data;
    state.loading.data = false
    state.billding = data.find(i => i.billding == 1)
    state.shipping = data.filter(i => i.billding != 1)
  },

  /**
   * Actualiza el progreso de consulta a la API
   * @param {Object} state
   * @param {Number} payload Progreso
   */
   PROGRESS (state, payload) {
    state.progress = payload
  },


  /**
   * Establce si se está consultando la API
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  LOADING (state, payload) {
    Vue.set(state.loading, 'data', payload.value);
  },

  /**
   * Actualizar los registro actuales con el último almacenado.
   *  Se agrega el registro al principio
   *
   * @param {Object} state
   * @param {Object} payload Último registro
   */
  STORE (state, payload) {
    state.data = payload;
    state.data = [ ...state.data ];
  },


  /**
   * Actualizar los registro actuales con el último actualizado
   *
   * @param {Object} state
   * @param {Object} payload Registro actualizado
   */
  UPDATE (state, payload) {
    state.data = payload;
    state.data = [ ...state.data ];
  },


  /**
   * Eliminar un registro
   *
   * @param {Object} state
   * @param {Object} payload ID del registro eliminado
   */
   DESTROY (state, payload) {
    var index = state.data.findIndex(item => item.id == payload)
    state.data.splice(index,1)
    state.data = [ ...state.data ]
    state.filterList = state.data
  },

  /**
   * Establece una petición al servidor
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  REQUEST (state, payload) {
    Vue.set(state.loading, payload, true);
  },

  /**
   * El que el servidor ha respondido
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  RESPONSE (state, payload) {
    Vue.set(state.loading, payload, false);
  },

}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
