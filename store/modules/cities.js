const state = {
  data: [],
}

// getters
const getters = {
  data: (state) => {
    return state.data
  }
}

// mutations
const mutations = {
  SET_DATA: (state, data) => {
    state.data = data;
  },

}

// actions
const actions = {
  async all ({ commit }) {
    if( state.data.length < 1 ) { // Veriicar si hay datos en cache
      await this.$api.cities.all().then(response => {
        commit('SET_DATA', response.data)
      });
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
