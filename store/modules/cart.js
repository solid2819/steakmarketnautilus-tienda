import Products from "./products";
import { $api } from "@/services";


const state = {
  products: Products.data,
  cart: [],
  service: $api['orders'],
  orders: []
}

// getters
const getters = {
  orders: (state) => {
    return state.orders
  },

  cartItems: (state) => {
    return state.cart
  },

  cartTotalAmount: (state) => {
    return state.cart.reduce((total, product) => {
      if( product.offer ) {
        return total + (parseFloat(product.offer_price) * product.quantity)
      } else {
        return total + (parseFloat(product.price) * product.quantity)
      }
    }, 0)
  }

}


// actions
const actions = {

  addToCart: (context, payload) => {
    context.commit('addToCart', payload)
  },

  updateCartQuantity: (context, payload) => {
    context.commit('updateCartQuantity', payload)
  },

  removeCartItem: (context, payload) => {
    context.commit('removeCartItem', payload)
  },

  async store ({ commit }, data) {
    return new Promise((resolve, reject) => { // Crear Promesa

      ///commit('REQUEST', 'data')

      //state.service.progress() // Habilitar preogreso de consulta a la API
      this.$api.orders.store(data)
        .then(response => {
          if( response.status == 200 ){ // Si se proceso con exito
            commit('STORE', response.data)
            //commit('PROGRESS', 0)
            //commit('RESPONSE', 'data')
            resolve(response)
          }
          else {
            //commit('PROGRESS', 0)
            //commit('RESPONSE', 'data')
            reject(response) // Se produjo un error
          }
        })
        .catch(error => {
          // eslint-disable-next-line no-console
          console.log(error);
          //commit('PROGRESS', 0)
          //commit('RESPONSE', 'data')
          reject(error);
        }); // catch
    });
  },

  async allByUser ({ commit }) {
    if( state.orders.length < 1) { // Veriicar si hay datos en cache
      await this.$api.orders.allByUser().then(response => {
          commit('SET_ORDERS', response)
      });
    }
  },

}

// mutations
const mutations = {

  SET_ORDERS(state, payload){
    state.orders = payload
  },

  STORE_ORDER(state, payload){

  },

  addToCart (state, payload) {

    const product = payload // state.products.find(item => item.id == payload.id);
    const cartItems = state.cart.find(item => item.id === product.id)
    const qty = product.quantity ? (product.quantity): 1

    if (cartItems) {
      cartItems.quantity += 1
    } else {
      state.cart.push({
        ...product,
        quantity: qty
      })
    }
    product.stock--
    
  },

  updateCartQuantity: (state, payload) => {
    // Calculate Product stock Counts
    function calculateStockCounts(product, quantity) {
      const qty = product.quantity + quantity
      const stock = product.stock
      if (stock < qty) {
        return false
      }
      return true
    }
    state.cart.find((items, index) => {
      if (items.id === payload.product.id) {
        const qty = state.cart[index].quantity + payload.qty
        const stock = calculateStockCounts(state.cart[index], payload.qty)
        if (qty !== 0 && stock) {
          state.cart[index].quantity = qty
        } else {
          // state.cart.push({
          //   ...product,
          //   quantity: qty
          // })
        }
        return true
      }
    })
  },

  removeCartItem: (state, payload) => {
    const index = state.cart.indexOf(payload)
    state.cart.splice(index, 1)
  }
}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
