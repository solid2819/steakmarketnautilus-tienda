const state = {
  /** Datos en cache */
  data: null,
  actives: false,
  inactives: false,
  default: false,
  current: false,
  usd: false,
  ves: false,

  /** Evita que se solicite la API multiples veces */
  loading: {
    data: false,
  }
}

const _default = {
  id: 1,
  title: "",
  code: "",
  symbol: "",
  cost: 1,
  default: true,
  enabled: 1,
}


// getters
const getters = {

  data (state){
    return state.data
  },

  actives (state){
    return state.actives
  },

  inactives (state){
    return state.inactives
  },

  current (state){
    return state.current
  },

  default (state){
    return state.default
  },

  usd (state){
    return state.usd
  },

  ves (state){
    return state.ves
  },

  changeCurrency: (state) => {
    return state.current
  },
}

// actions
const actions = {
  async all({ commit }) {
    // Veriicar si hay datos en cache y que tampoco se esté ejecutando una llamada a la API
    if( state.data == null && state.loading.data == false) {
        state.loading.data = true // Bloquear futuras llamadas a la API
        await this.$api.currency.all().then(response => {
            commit('SET_DATA', response.data)
        });
    }
  },

  changeCurrency: (context, payload) => {
    context.commit('changeCurrency', payload)
  },

}

// mutations
const mutations = {
  SET_DATA(state, data) {
    if( data ) {
      state.data = data

      state.loading.data = false
      state.actives = data.filter( item => item.enabled );
      state.inactives = data.filter( item => !item.enabled );
      state.default = data.filter( item => item.default );
      state.usd = data.filter( item => item.code == 'USD' )[0];
      state.ves = data.filter( item => item.id == 1 )[0];

      state.current = state.default[0];
    }
  },

  changeCurrency: (state, payload) => {
    state.current = payload
  },

}

//export store module
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
