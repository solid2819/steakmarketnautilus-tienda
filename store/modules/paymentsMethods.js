const state = {
  data: [],
  efectivo: [],
  zelle: [],
  transferencia: [],
  pagoMovil: [],
  tdd: [],
  tdc: [],
  tddi: [],
  tdci: [],  
  paypal: [],
  loading: {
    data: false,
  },
}

// getters
const getters = {

  data: (state) => {
    return state.data;
  },

  stores: (state) => {
    return state.stores;
  },

  zelle: (state) => {
    return state.zelle;
  },

  efectivo: (state) => {
    return state.efectivo;
  },
  
  transferencia: (state) => {
    return state.transferencia;
  },  

  pagoMovil: (state) => {
    return state.pagoMovil;
  },

  paypal: (state) => {
    return state.paypal;
  },

  tdd: (state) => {
    return state.tdd;
  },

  tdc: (state) => {
    return state.tdc;
  },

  tddi: (state) => {
    return state.tddi;
  },

  tdci: (state) => {
    return state.tdci;
  },
}

// actions
const actions = {

  async all ({ commit }) {
    if( state.data.length < 1 && state.loading.data == false) { // Veriicar si hay datos en cache
      commit('SET_LOADING', {loading: 'data', value: true}) // Impedir nuevas solicitudes
      await this.$api.paymentMethods.all().then(response => {
          commit('SET_DATA', response.data.data)
      });      
    }
  },

}


// mutations
const mutations = {
  SET_LOADING: (state, data) => {
    state.loading[data.loading]  = data.value
  },

  SET_DATA: (state, data) => {
    state.data = data;

    state.efectivo = data.filter(item => item.type == 'efectivo');
    state.zelle = data.filter(item => item.type == 'zelle');
    state.transferencia = data.filter(item => item.type == 'transferencia');
    state.pagoMovil = data.filter(item => item.type == 'pago-movil');
    state.tdd = data.filter(item => item.type == 'tdd');
    state.tdc = data.filter(item => item.type == 'tdc');
    state.tddi = data.filter(item => item.type == 'tddi');
    state.tdci = data.filter(item => item.type == 'tdci');  
    state.paypal = data.filter(item => item.type == 'paypal');

    state.loading.data = false
  },
}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}