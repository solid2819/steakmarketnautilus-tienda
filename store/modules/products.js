import Vue from 'vue';
import Config from "../../data/config.json";
import Currency from './currency'


const state = {

  loading: {
    allProducts: false,
    products: false,
    productsNews: false,
    productsOffers: false,
    categories: false,

    productsByCategory: false,
    product: false
  },

  search: [],

  products: [],
  productsNews: [],
  productsOffers: [],
  productsList: [],
  filteredProduct: [],
  filteredProductByCategory: [],

  productsByCategory:[],
  product: null,


  currentCategory: '',
  categories: [],

  wishlist: [],
  compare: [],
  currency: Currency.state.current,
  order: [],
  locale: 'es',
  searchProduct: [],
  api: Config.config.api
}

// getters
const getters = {

  loadingAllProducts: (state) => {
    return state.loading.allProducts;
  },


  loadingProductsByCategory: (state) => {
    return state.loading.productsByCategory;
  },
  productsByCategory: (state) => {
    return state.productsByCategory;
  },


  loadingProduct: (state) => {
    return state.loading.product;
  },
  product: (state) => {
    return state.product;
  },



  products: (state) => {
    return state.products;
  },

  productsNews: (state) => {
    return state.productsNews;
  },

  productsOffers: (state) => {
    return state.productsOffers;
  },

  productsList: (state) => {
    return state.productsList;
  },

  filteredProductByCategory: (state) => {
    return state.filteredProductByCategory;
  },




  categories: (state) => {
    return state.categories;
  },

  currentCategory: (state) => {
    return state.currentCategory ? state.currentCategory : {side_banner: '', main_banner: ''};
  },


  getcollectionProduct: (state) => {
    return collection => state.products.filter((product) => {
      return collection === product.collection
    })
  },

  getProductBySlug: (state) => (slug) => {
    return state.products.find(product => {
      return product.slug === slug
    })
  },

  getProductById: (state) => {
    return id => state.products.find(product => {
      return product.id === +id
    })
  },

  compareItems: (state) => {
    return state.compare
  },

  wishlistItems: (state) => {
    return state.wishlist
  },

  changeCurrency: (state) => {
    return state.currency
  },

  getOrder: (state) => {
    return state.order
  },

  filterProducts: (state) => {

    return state.filteredProduct.filter((product) => {

      if (!state.tagItems.length) return true

      const Tags = state.tagItems.some((prev) => { // Match Tags
        if (product.tags) {
          if (product.tags.includes(prev)) {
            return prev
          }
        }
      })
      return Tags

    });
  }
}

// actions
const actions = {

  async all ({ commit, state }) {
    return new Promise((resolve, reject) => {

      if( state.products.length < 1 && state.loading.allProducts == false ) { // Veriicar si hay datos en cache
        commit('REQUEST', 'allProducts') // Impedir nuevas solicitudes
        this.$api.products.all()
          .then(response => {
            commit('SET_PRODUCTS', response.data)
            commit('RESPONSE', 'allProducts')
            resolve(response)
          });
      } else {
        resolve(true)
      }
    });
  },

  /**
   * Obtiene los productos de una categoría específica
   * @param
   * @returns
   */
  async productsByCategory ({ commit, state }, slug) {
    return new Promise((resolve, reject) => {
      if( state.loading.productsByCategory == false ) { // Veriicar si hay datos en cache
        commit('REQUEST', 'productsByCategory') // Impedir nuevas solicitudes
        this.$api.products.productsByCategory(slug)
          .then(response => {
            commit('PRODUCTS_BY_CATEGORY', response.products)
            commit('RESPONSE', 'productsByCategory')
            resolve(response)
          });
      } else {
        resolve(true)
      }
    });
  },

  /**
   * Obtiene los productos de una categoría específica
   * @param
   * @returns
   */
  async bySlug ({ commit, state }, slug) {
    return new Promise((resolve, reject) => {
      if( state.loading.product == false ) { // Veriicar si hay datos en cache
        commit('PRODUCT', null)
        commit('REQUEST', 'product') // Impedir nuevas solicitudes
        this.$api.products.bySlug(slug)
          .then(response => {
            commit('PRODUCT', response.data)
            commit('RESPONSE', 'product')
            resolve(response)
          });
      } else {
        resolve(true)
      }
    });
  },

  async search ({ commit, state }, payload) {
    if(payload.trim() == '' || payload.length < 3){
      commit('SEARCH', [])
      return
    }

    let data = {
      search: payload
    }
    return new Promise((resolve, reject) => {
      //if( state.loading.product == false ) { // Veriicar si hay datos en cache
        //commit('PRODUCT', null)
        //commit('REQUEST', 'product') // Impedir nuevas solicitudes
        this.$api.products.search(data)
          .then(response => {
            commit('SEARCH', response.data)
            //commit('RESPONSE', 'product')
            resolve(response)
          });
      //} else {
       // resolve(true)
     // }
    });
  },



  async getProductsNews ({ commit, state }) {
    return new Promise((resolve, reject) => {
      if( state.productsNews.length < 1 && state.loading.products == false) { // Veriicar si hay datos en cache
        commit('REQUEST', 'productsNews') // Impedir nuevas solicitudes
        this.$api.products.productsNews().then(response => {
          commit('SET_PRODUCTS_NEWS', response.data)
          commit('RESPONSE', 'productsNews')
          resolve(response)
        });
      }
    });
  },

  async getProductsOffers ({ commit, state }) {
    var v = 0;
    return new Promise((resolve, reject) => {
      if( state.productsOffers.length < 1 ) { // Veriicar si hay datos en cache
        commit('REQUEST', 'productsOffers') // Impedir nuevas solicitudes
        this.$api.products.getProductsOffers().then(response => {
          commit('SET_PRODUCTS_OFFERS', response.data)
          commit('RESPONSE', 'productsOffers')
          resolve(response)
        });
      }
    });
  },

  async getAllCategories({ commit, state }) {
    return new Promise((resolve, reject) => {
      if( state.categories.length < 1 && state.loading.categories == false) { // Veriicar si hay datos en cache
        commit('REQUEST', 'categories') // Impedir nuevas solicitudes
          this.$api.categories.all().then(response => {
            commit('SET_CATEGORIES', response.data)
            commit('RESPONSE', 'categories')
            resolve(response)
        });
      }
    });
},

  async getProductsOfCategory({ commit, state }, category_slug, init = false) {
    if(init) init ()
    commit('SET_PRODUCTS_OF_CATEGORY', category_slug)
  },

  changeCurrency: (context, payload) => {
    context.commit('changeCurrency', payload)
  },

  addToWishlist: (context, payload) => {
    context.commit('addToWishlist', payload)
  },

  removeWishlistItem: (context, payload) => {
    context.commit('removeWishlistItem', payload)
  },

  addToCompare: (context, payload) => {
    context.commit('addToCompare', payload)
  },

  removeCompareItem: (context, payload) => {
    context.commit('removeCompareItem', payload)
  },

  searchProduct: (context, payload) => {
    context.commit('searchProduct', payload)
  },


  createOrder: (context, payload) => {
    context.commit('createOrder', payload)
  },

  sortProducts: (context, payload) => {
    context.commit('sortProducts', payload)
  },

  getCategoryFilter: (context, payload) => {
    context.commit('getCategoryFilter', payload)
  },
}


// mutations
const mutations = {

  /**
 * Establece una petición al servidor
 *
 * @param {Object} state
 * @param {Number} payload Estatus del load
 */
  REQUEST (state, payload) {
    Vue.set(state.loading, payload, true);
  },

  /**
   * El que el servidor ha respondido
   *
   * @param {Object} state
   * @param {Number} payload Estatus del load
   */
  RESPONSE (state, payload) {
    Vue.set(state.loading, payload, false);
  },


  SET_LOADING: (state, data) => {
    state.loading[data.loading] = data.value
  },

  SET_PRODUCTS: (state, data) => {
    state.products = data;
    state.productsList = data;
  },

  SEARCH: (state, data) => {
    state.search = data;
  },

  clearSearch:(state, data) => {
    state.search = [];
  },

  SET_PRODUCTS_NEWS: (state, data) => {
    state.productsNews = data
  },

  SET_PRODUCTS_OFFERS: (state, data) => {
    state.productsOffers = data;
  },

  changeCurrency: (state, payload) => {
    currency.state.current = payload
  },

  addToWishlist: (state, payload) => {
    /*const product = state.products.find(item => item.id === payload.id)
    const wishlistItems = state.wishlist.find(item => item.id === payload.id)
    if (wishlistItems) {
    } else {
      state.wishlist.push({
        ...product
      })
    }*/
  },

  removeWishlistItem: (state, payload) => {
    /*const index = state.wishlist.indexOf(payload)
    state.wishlist.splice(index, 1)*/
  },

  addToCompare: (state, payload) => {
    /*const product = state.products.find(item => item.id === payload.id)
    const compareItems = state.compare.find(item => item.id === payload.id)
    if (compareItems) {
    } else {
      state.compare.push({
        ...product
      })
    }*/
  },

  removeCompareItem: (state, payload) => {
    const index = state.compare.indexOf(payload)
    state.compare.splice(index, 1)
  },

  search(state, payload){
    state.searchProduct = payload
  },

  searchProduct: (state, payload) => {
    payload = payload.toLowerCase()
    state.searchProduct = []

    if (payload.length) {
      state.products.filter((product) => {
        if (product.title.toLowerCase().includes(payload)) {
          state.searchProduct.push(product)
        }
      })
    }
  },

  createOrder: (state, payload) => {
    state.order = payload
  },

  sortProducts: (state, payload) => {
    if (payload === 'a-z') {
      state.productsList.sort(function (a, b) {
        if (a.title < b.title) {
          return -1
        } else if (a.title > b.title) {
          return 1
        }
        return 0
      })
    } else if (payload === 'z-a') {
      state.productsList.sort(function (a, b) {
        if (a.title > b.title) {
          return -1
        } else if (a.title < b.title) {
          return 1
        }
        return 0
      })
    } else if (payload === 'low') {
      state.productsList.sort(function (a, b) {
        if (a.price < b.price) {
          return -1
        } else if (a.price > b.price) {
          return 1
        }
        return 0
      })
    } else if (payload === 'high') {
      state.productsList.sort(function (a, b) {
        if (a.price > b.price) {
          return -1
        } else if (a.price < b.price) {
          return 1
        }
        return 0
      })
    }
  },


  PRODUCTS_BY_CATEGORY: (state, data) => {
    state.productsByCategory = data
  },

  PRODUCT: (state, data) => {
    state.product = data
  },

  SET_PRODUCTS_OF_CATEGORY: (state, payload) => {

    state.filteredProductByCategory = []
    state.tagItems = []
    try {
      state.currentCategory = state.categories.find( item => item.slug === payload )

      state.products.filter((product) => {
        if( product.category ) {
          if (payload === product.category.slug) {
            state.filteredProductByCategory.push(product)
            state.priceArray = state.filteredProductByCategory
          }
        }
        if (payload === 'all' || payload === undefined) {
          state.filteredProductByCategory.push(product)
          state.priceArray = state.filteredProductByCategory
        }
      })
    } catch(e) {

    }

  },

  SET_CATEGORIES: (state, payload) => {
    state.categories = payload;
    state.loading.categories = false
  },


  getCategoryFilter: (state, payload) => {

    state.filteredProduct = []
    state.tagItems = []

    state.products.filter((product) => {
      if (payload === product.category.slug) {
        state.filteredProduct.push(product)
        state.priceArray = state.filteredProduct
      }
      if (payload === 'all' || payload === undefined) {
        state.filteredProduct.push(product)
        state.priceArray = state.filteredProduct
      }
    })
  },

}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  data: state.products,
}
