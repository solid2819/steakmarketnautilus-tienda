//to handle state
const state = {
    data: null,
    sliders: [],
    contact: false,
    minimum_amount: false,
    about:false,
    privacy:false,
    rrss: false,
    homeBanner: null,
    delivery: null,
    terminos: null,

    /** Evita que se solicite la API multiples veces */
    loading: false
}

//to handle state
const getters = {
  data (state){
     return state.data
  },

  contact (state){
     return state.contact
  },

  rrss (state){
    return state.rrss
  },

  sliders (state){
    return state.sliders
  },

  homeBanner (state){
    return state.homeBanner
  },

  about(state){
    return state.about
  },

  privacy(state){
    return state.privacy
  },

  delivery(state){
    return state.delivery ? state.delivery.value : null
  },

  terminos(state){
    return state.terminos ? state.terminos.value : null
  }

}

//to handle actions
const actions = {
    async all({ commit }) {
        if( state.data == null && state.loading == false) { // Veriicar si hay datos en cache
            state.loading = true
            await this.$api.setting.all().then(response => {
                commit('SET_DATA', response.data)
                state.loading = false
            });
        }
    }
}

//to handle mutations
const mutations = {
  SET_DATA(state, data) {
    if( data ) {
      state.data = data
      state.sliders = data.find( item => item.name == 'sliders' ).value
      state.contact = data.find( item => item.name === 'contact' ).value
      state.minimum_amount = data.find( item => item.name === 'minimumAmount' ).value
      state.rrss = data.find( item => item.name === 'rrss' ).value
      state.homeBanner = data.find( item => item.name === 'homeBanner' ).value

      state.terminos = data.find( item => item.name === 'page.terminos' )
      state.about = data.find( item => item.name === 'page.about' ).value
      state.privacy = data.find( item => item.name === 'page.privacy' ).value
      state.delivery = data.find( item => item.name === 'page.delivery' )
    }
  }
}

//export store module
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
