
import Model from '../helpers/models/Model'

export default class MenuService extends Model {

  /** @param {Object} _data Todos los recursos */
  _data;

  _categories;

  constructor() {
    super('menu');
  }
  
  /** Retorna todos los registros en cache menú principal*/
  get data() {
    return this._data
  }

  set data(value) {
    this._data = value
  }

  async getMenu()
  {
    this._data = await this.query('menu')
    return this._data;
  }



  /** Retorna todos los registros en cache del menú de categorías*/
  get categories() {
    return this._categories
  }

  set categories(value) {
    this._categories = value
  }

  /**
   * Retorna el menú de categorías
   * @returns {Promise}
   */
  async getCategories()
  {
    return await this.query('menu/categories');
  }




}