
import Model from '../helpers/models/Model'

export default class CategoryService extends Model {

  /** @param {Object} _data Todos los recursos */
  _data = null

  constructor() {
    super('categories');
  }
  
  /** Retorna todos los registros en cache */
  get data() {
    return this._data    
  }

  /** Retorna todos los registros en cache del menú de categorías*/
  get categoriesRoot() {
    return this._categoriesRoot
  }

  set categoriesRoot(value) {
    this._categoriesRoot = value
  }

  /**
   * Retorna el menú de categorías
   * @returns {Promise}
   */
  async getCategoriesRoot()
  {
    return await this.query('categories/root');
  }

    

}