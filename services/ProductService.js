
import Model from '../helpers/models/Model'

export default class ProductService extends Model {

  _productsOffers = null
  _productsNews = null

  constructor() {
    super('products');
  }

  get productsOffers() {
    return this._productsOffers
  }

  set productsOffers(value) {
    return this._productsOffers = value
  }

  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
  async getProductsOffers(count = 10)
  {
    return await this.query('products/offers?count=' + count)
  }


  get productsNews() {
    return this._productsNews
  }

  set productsNews(value) {
    return this._productsNews = value
  }

  /**
 * Retorna los productos en oferta por orden alfabetico
 * @param {Number} count Determina la cantidad de productoss
 * @returns
 */
  async todos()
  {
    return await this.query('productos')
  }

  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
  async getProductsNews(count = 10)
  {
    return await this.query('productos/news?count=' + count)
  }


  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
   async productsByCategory(slug)
   {
     return await this.query(`categories/${slug}/products?withstock=true`)
   }


  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
  async bySlug(slug)
  {
    return await this.query(`products/slug/${slug}`)
  }


  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
  async search(data)
  {
    return await this.query(`products/search`, 'post', data)
  }

}
