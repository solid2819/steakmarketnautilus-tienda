import Model from '../helpers/models/Model'
import ls from '@/helpers/localStorage'

const user = ls.get('auth-user') || '{}';

export default class OrdersServices extends Model {

  constructor() {
    super('orders');
  }

  async allByUser() {
    return await this.query(`/user/${user.id}/orders`)
  }

}
