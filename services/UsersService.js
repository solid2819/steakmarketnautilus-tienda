
import {Authenticatable} from '../helpers/models/Authenticatable'

export default class UsersService extends Authenticatable {

  constructor() {
    super('users');
  }
  
}
