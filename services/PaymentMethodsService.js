
import Model from '../helpers/models/Model'

/**
 * Servicio de Monedas
 * 
 * @author Brayan Rincon <brayan262@gmail.com>
 * @extends Model
 * @version 1.0.0
 * 
 * ```js
 * // Devuelve todos los registros
 * instance.all().then( response => { if( response.status == '200' ) return true; } )
 * ```
 */
export default class PaymentMethodsService extends Model {

  constructor() {
    super('payment-methods');
  }
  
}