
import Model from '../helpers/models/Model'
import ls from '@/helpers/localStorage'

const user = ls.get('auth-user') || '{}';


export default class AddressesServices extends Model {

  constructor() {
    super('address');
  }

  async getAddressByUser() {
    return await this.query(`/user/${user.id}/address`)
  }

}
