
import Model from '../helpers/models/Model'

/**
 * Servicio de Monedas
 * 
 * @author Brayan Rincon <brayan262@gmail.com>
 * @extends Model
 * @version 1.0.0
 * 
 * ```js
 * // Devuelve todos los registros
 * instance.all().then( response => { if( response.status == '200' ) return true; } )
 * ```
 */
export default class CurrencyService extends Model {

  constructor() {
    super('currency');
  }
  
  /** Retorno los datos almacenados en cache */
  get data() {
    return this._data
  }

  /** Establece los datos que se almacenarán en cache */
  set data(value) {
    return this._data = value
  }

}