
import Model from '../helpers/models/Model'

export default class SettingService extends Model {

  constructor() {
    super('settings');
  }
  
  /** Retorno los datos almacenados en cache */
  get data() {
    return this._data
  }

  /** Establece los datos que se almacenarán en cache */
  set data(value) {
    return this._data = value
  }

}