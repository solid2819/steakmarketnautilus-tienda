//import APIService from './APIService'
import ProductService from './ProductService'
import CategoryService from './CategoryService'
import MenuService from './MenuService'
import SettingService from './SettingService'
import CurrencyService from './CurrencyService'
import AddressesServices from './AddressesServices'
import PaymentMethodsService from './PaymentMethodsService'
import OrdersServices from './OrdersServices'
import UsersService from './UsersService'
import CitiesService from './CitiesService'
import StatesService from './StatesService'
import CountriesService from './CountriesService'


export const $api = {
    //query: new APIService(),
    product: new ProductService(),

    category: new CategoryService(),
    menu: new MenuService(),
    setting: new SettingService(),
    currency: new CurrencyService(),
    address: new AddressesServices(),
    payment: new PaymentMethodsService(),

    products: new ProductService(),
    categories: new CategoryService(),
    settings: new SettingService(),
    currencies: new CurrencyService(),
    addresses: new AddressesServices(),
    paymentMethods: new PaymentMethodsService(),
    orders: new OrdersServices(),
    users: new UsersService(),
    states: new StatesService(),
    cities: new CitiesService(),
    countries: new CountriesService(),
};


export const API = {
  products: new ProductService(),
  categories: new CategoryService(),
  settings: new SettingService(),
  currencies: new CurrencyService(),
  addresses: new AddressesServices(),
  paymentMethods: new PaymentMethodsService(),
  orders: new OrdersServices(),
  users: new UsersService(),
  states: new StatesService(),
  cities: new CitiesService(),
  countries: new CountriesService(),
};
