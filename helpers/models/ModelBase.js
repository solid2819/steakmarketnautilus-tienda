import Config from "../../data/config.json";
import HttpClient from '../http/HttpClient';

const state = {
  api: Config.config.apiURL,
  apiGetToken: Config.config.api,
  slug: Config.config.slug,
}

export default class ModelBase {

  _baseUrl;
  _service;
  _slugAPI;
  _csrf_token;
  _lastError;

  /**
   *
   * @param {*} service
   */
  constructor(service = '') {

    if (!service) throw new Error("Service is not provided");

    this._service = service;
    this._baseUrl = state.api + '/';
    this._urlGetToken = state.apiGetToken + '/';
    this._slugAPI = state.slug.api != '' ? state.slug.api + '/' : '';


    HttpClient.defaults.baseURL = this.endpoint();

    HttpClient.interceptors.response.use(response => {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    });

    HttpClient.defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin': '*'
      //'X-CSRF-TOKEN': this.csrf_token // document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    };

  }


  /**
   *
   */
  get http(){
    return HttpClient;
  }

  /**
   *
   */
  set http(value){
    return HttpClient(value);
  }

  /**
   *
   * @param {*} resource
   * @returns
   */
  endpoint(resource = '') {
    return `${this._baseUrl}${this._slugAPI}${resource}`;
  }

  /**
   *
   * @param {*} error
   */
  handleErrors(error) {
    const ex = {
      service: this._service,
      exception: error,
    }
    console.log(ex); // console
    return ex
  }
}
