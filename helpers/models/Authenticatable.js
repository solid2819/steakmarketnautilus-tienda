
import ls from '@/helpers/localStorage'
import Model from './Model'

const TOKEN = ls.get('auth-token', false) || null;
const USER = ls.get('auth-user') || '{}';

export class Authenticatable extends Model {

    /**
     * Constructor
     * @param {String} service Nombre del Servicio/Endpoint
     * @param {String} store Opcional. Nombre del módulo en el store
     */
    constructor(service, store = false) {
      super(service, store);
    }

  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Number} count Determina la cantidad de productoss
   * @returns
   */
  async login( credential = {} )
  {
    return this.getToken().then( () => this.query(`login`, 'post', credential) )
  }

  async logout()
  {
    return this.getToken().then( () => this.query(`logout`, 'post') )
  }

  async forgot(email){
    return await this.query(`password/forgot`, 'post', {email: email})
  }

  async reset(data){
    return await this.query(`password/reset`, 'post', data)
  }
  
  /**
   * Retorna los productos en oferta por orden alfabetico
   * @param {Object} credential {email: 'mail@mail.com', password: '123456', password_confirmation: '123456'}
   * @returns
   */
  async register( credential = {} )
  {
    this.withCredentials(false)
    return this.getToken().then(response => {
      return this.query(`users/register`, 'post', credential).then(response => response)
    })
  }

}
