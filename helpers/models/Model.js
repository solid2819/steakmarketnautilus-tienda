import ls from '@/helpers/localStorage'
import ModelBase from './ModelBase'
import { HttpErrorMessage } from "@/helpers/http/HttpErrors";

const TOKEN = ls.get('auth-token', false) || null;
const USER = ls.get('auth-user') || false;

export default class Model extends ModelBase {

/**@param {String} _service Nombre del sericio */
_service;

/**@param {String} _module Nombre del módulo en el store */
_module;

/** @param {Object} _data Todos los recursos */
_data = null;

/** @param {Boolean} _progress Configuraciones adicionales del de axios */
_progress;

/** @param {Number} _progressUpload Estado del Progress */
_progressUpload;

/** @param {Object} _config Configuraciones adicionales del de axios */
_config = {};

/**
 * Constructor
 * @param {String} service Nombre del Servicio/Endpoint
 * @param {String} store Opcional. Nombre del módulo en el store
 */
constructor(service, module = false) {
  super(service);
  this._service = service;
  this._module = module || service;

  let self = this

  this._config = {
    onUploadProgress: e => {
      self._progressUpload = Math.round((e.loaded * 100) / e.total)
      store.dispatch(`${self._module}/progress`, self._progressUpload)
    }
  }
}

get serviseName(){
  return this._service
}

getHeaderAuthorization(){
  return TOKEN ? { Authorization: `Bearer ${TOKEN}` } : {}
}

getUserAuth(){
  return USER
}


async getToken() {

  this.http.defaults.baseURL = this._urlGetToken;

  let self = this, config = {
    method: 'get',
    url: 'sanctum/csrf-cookie',
  }

  return new Promise((resolve, reject) => {
    self.http( config )
      .then( response => {
        resolve(response)
      })
      .catch( error => {
        reject(error)
      });
  }); // Promise

}

/**
 * Ejecuta un consulta a la API personalizada
 *
 * @param {String} url URL del servidor que se utilizará para la solicitud
 * @param {String} method Método de solicitud que se utilizará al realizar la solicitud
 * @param {Object} data Datos que se enviarán como cuerpo de la solicitud. Solo aplicable para los métodos de solicitud 'PUT', 'POST', 'DELETE y' PATCH
 * @returns {Promise} Retorna una promesa
 */
async query(url, method = 'get', data = {}, header = {}) {

  this.http.defaults.baseURL = this.endpoint();

  let self = this, config = {
    method: method,
    url: url,
    data: data,
    header: header,
  }


  if(this._progress == true){
    config = { ...config, ...{onUploadProgress: this._config.onUploadProgress} }
  }

  return new Promise((resolve, reject) => {
    self.http( config )
      .then( response => {
        self._progress = false
        resolve(self.processResponse(response))
      })
      .catch( error => {
        self._progress = false
        reject(self.processError(error))
      });
  }); // Promise

}



/**
 *
 * @returns
 */
async all() {
  try {
    let self = this
    return new Promise((resolve, reject) => {
      this.query( this.endpoint(this._service), 'get' )
        .then( response => {
          resolve(response)
        })
        .catch( error => {
          reject(error)
        });
    }); // Promise
  } catch (ex) {
      this.handleErrors(ex);
  }
}

/**
 *
 * @param {*} id
 * @returns
 */
async get(id) {
  try {
      if (!id) throw Error("Id is not provided");
      return await this.query( `${this.endpoint(this._service)}/${id}`, 'get' )
  } catch (ex) {
      this.handleErrors(ex);
  }
}

/**
 *
 * @param {*} data
 * @returns
 */
async store(data = {}) {
  try {
    let self = this
    return new Promise((resolve, reject) => {
      this.query( `${this.endpoint(this._service)}`, 'post', data )
        .then( response => {
          resolve(response)
        })
        .catch( error => {
          reject(error)
        });
    }); // Promise
  } catch (ex) {
    this.handleErrors(ex);
  }
}

/**
 * Actualizar los campos de un registro
 * @param {Number} id ID del registro
 * @param {Object} data Campos del registro
 * @returns
 */
async update(id, data = {}) {
  if (!id) throw Error("Id is not provided in Service: " + this._service);
  try {
    let self = this
    return new Promise((resolve, reject) => {
      this.query( `${this.endpoint(this._service)}/${id}`, 'put', data )
        .then( response => {
          resolve(response)
        })
        .catch( error => {
          reject(error)
        });
    }); // Promise
  } catch (ex) {
      this.handleErrors(ex);
  }
}

/**
 *
 * @param {*} id
 * @returns
 */
async destroy(id) {
  try {
      if (!id) throw Error("Model: id is not provided");

      let self = this
      return new Promise((resolve, reject) => {
        this.query( `${this.endpoint(this._service)}/${id}`, 'delete' )
          .then( response => {
            resolve(response)
          })
          .catch( error => {
            reject(error)
          });
      }); // Promise
  } catch (ex) {
      this.handleErrors(ex);
  }
}

async destroyes(ids) {
  try {
      if (!ids) throw Error("Model: ids is not provided");
      return new Promise((resolve, reject) => {
        this.query( `${this.endpoint(this._service)}`, 'delete', ids )
          .then( response => {
            resolve(response)
          })
          .catch( error => {
            reject(error)
          });
      }); // Promise
  } catch (ex) {
      this.handleErrors(ex);
  }
}


/**
 *
 */
progress(){
  this._progress = true
  store.dispatch(`${this._module}/progress`, 0)
}

/**
 *
 * @returns
 */
progressUpload(){
  return this._progressUpload
}

processResponse(response) {
  /**
   * Respuestas informativas (100–199)
   * Respuestas satisfactorias (200–299)
   * Redirecciones (300–399)
   */
  return response.data
}

processError(error) {

  /**
   * Errores de los clientes (400–499)
   * Errores de los servidores (500–599).
   */
  if (error.response) {
    // Se realizó la solicitud y el servidor respondió con un código
    // de estado que cae fuera del rango de 2xx

    if( error.response.status == 500 ) {
      var _data =  error.response.data
      var _message = `${_data.message}. \nLine: ${_data.line}. File: ${_data.file}.`

      return {
        status: error.response.status,
        httpError: HttpErrorMessage(error.response.status),
        data: {
          message: _message
        }
      }
    }


    return {
      status: error.response.status,
      httpError: HttpErrorMessage(error.response.status),
      data: error.response.data
    }
  }
  else if (error.request) {
    // Se hizo la solicitud pero no se recibió respuesta
    // `error.request` es una instancia de XMLHttpRequest en el navegador
    console.log(error.request);
  }
  else {
    // Algo sucedió al configurar la solicitud que desencadenó un error
    console.log('Error', error.message);
  }
}

}
