
export default {
  mode: 'spa',
  subdirectory: '/',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Steak Market Nautilus',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      { name: "apple-mobile-web-app-title", content:"Steak Maket Nautilus"},
      { name: "application-name", content:"Steak Maket Nautilus"},
      { name:"msapplication-TileColor", content:"#f1f1f1" },
      { name: "theme-color", content:"#ffffff"}
    ],
    link: [
      { rel: "apple-touch-icon", sizes:"180x180", href: "/apple-touch-icon.png"},
      { rel: "icon", type:"image/png", sizes:"32x32", href:"/favicon-32x32.png" },
      { rel: "icon", type:"image/png", sizes:"16x16", href:"/favicon-16x16.png" },
      { rel: "manifest", href:"/site.webmanifest" },
      { rel: "mask-icon", href:"/safari-pinned-tab.svg", color:"#a53333" },
      { rel: 'icon', type: 'image/x-icon', href: '/multikartvue/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900' }
    ],
    script: [
      //{ src: 'https://checkout.stripe.com/checkout.js'}
    ]
  },

  router: {
    base: '/'
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#ff4c3b', throttle: 200, height: '3px', css: true },

  
  loadingIndicator: '~/components/loading.html',
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/app.scss',
    'swiper/swiper.min.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
     { src: '~/plugins/plugin.js', ssr:false },
     { src: '~/plugins/localStorage.js', ssr:false },
     { src: '~/plugins/mixins.js', ssr:false },
     { src: '~/plugins/vue2-filters', ssr:false },
     { src: '~/plugins/route', ssr:false }
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    'vue-scrollto/nuxt',
    'vue-sweetalert2/nuxt'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  generate: {
    fallback: true
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
